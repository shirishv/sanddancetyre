<?php
require_once 'app/Mage.php';
Mage::app();
$allCategories = Mage::getModel ( 'catalog/category' );
$categoryTree = $allCategories->getTreeModel();
$categoryTree->load();
$categoryIds = $categoryTree->getCollection()->getAllIds ();	

if ($categoryIds) {
	$outputFile = "var/importexport/categories-and-ids.csv";
	$write = fopen($outputFile, 'w');
	
	$Header = array('Category Name', 'Category Id');
	fputcsv($write, $Header);
	
	foreach ( $categoryIds as $categoryId ) {
		$data = array($allCategories->load($categoryId)->getName(), $categoryId);
		fputcsv($write, $data);
	}?>
	<a href="http://www.sanddancetyre.com/var/importexport/categories-and-ids.csv">Download</a>
<?php }
fclose($write);
?>