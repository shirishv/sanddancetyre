/**
 * sellerDeck_CustomReview.js - Implementation file for SellerDeck Payment Ecommerce class.
 * 
 * @package     SellerDeck Payment Extensions
 * 
 * @author      Consultant
 * @copyright   © SellerDeck Ltd 2016. All rights reserved.
 */
var CustomReview = Class.create();
CustomReview.prototype = {
    initialize: function(saveUrl, getOrderUrl, successUrl, agreementsForm, module){
        this.saveUrl = saveUrl;
		this.getOrderUrl = getOrderUrl;
        this.successUrl = successUrl;
        this.agreementsForm = agreementsForm;
        this.onSave = this.nextStep.bindAsEventListener(this);
		this.code  = module;
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },
	getPaymentMethod: function()
		{
		var elements 	= $$("#checkout-payment-method-load [name='payment[method]']");
		for(var i=0; i<elements.length; i++)
			{
			if(elements[i].checked) 
				{
				return elements[i].value;
				}
			}
		return null;
		},
	isSellerPaymentMethod: function()
		{
		return (this.getPaymentMethod() === this.code);
		},
	evalTransport: function(transport)
		{
        try {
            response = eval('('+transport.responseText+')')
			}
		catch(e)
			{
            response = {}
			}
    return response;
		},
    save: function()
		{
		var params = 'payment[method]=paymentecommerce';
        // params.save = true;
		console.log(params);
		var request =  new Ajax.Request(
            this.saveUrl,
            {
                method:'post',
                parameters:params,
                onComplete: this.onComplete,
                onSuccess: this.onSave,
			}
        );
        
		},

    resetLoadWaiting: function(transport)
		{
        // checkout.setLoadWaiting(false, this.isSuccess);
		},

    nextStep: function(transport)
		{
        if (transport && transport.responseText)
			{
            try
				{
                response = eval('(' + transport.responseText + ')');
				}
            catch (e)
				{
                response = {};
				}
			if (response.redirect)
				{
                this.isSuccess = true;
                return;
				} 
			if (response.success)
				{
                this.isSuccess = true;
				new Ajax.Request(this.getOrderUrl, {
				method:"post",
				parameters:{data:response},
				onSuccess:function(transport){
					var response = this.evalTransport(transport);
					$('ekashu_reference').value = response.order_increment_id;
					$('ekashu_hash_code').value = response.hash_code;
					setTimeout(document.getElementById("SellerDeckPayment").submit(),100);
				}.bind(this)
				});
				return false;
				}
            else
				{
                var msg = response.error_messages;
                if (typeof(msg)=='object')
					{
                    msg = msg.join("\n");
					}
                if(msg)
					{
                    alert(msg);
					}
				}

            if (response.update_section)
				{
                $('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
				}

            if (response.goto_section)
				{
                // checkout.gotoSection(response.goto_section, true);
				}
			}
		},

		isSuccess: false
}
