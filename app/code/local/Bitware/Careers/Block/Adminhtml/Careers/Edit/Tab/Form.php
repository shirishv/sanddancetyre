<?php

class Bitware_Careers_Block_Adminhtml_Careers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('careers_form', array('legend'=>Mage::helper('careers')->__('Item information')));
     

      $fieldset->addField('firstname', 'label', array(
          'label'     => Mage::helper('careers')->__('First Name'),
          'required'  => true,
          'name'      => 'firstname',
      ));
      $fieldset->addField('lastname', 'label', array(
          'label'     => Mage::helper('careers')->__('Last Name'),
          'required'  => true,
          'name'      => 'lastname',
      ));
      $fieldset->addField('email', 'label', array(
          'label'     => Mage::helper('careers')->__('E-mail Address'),
          'required'  => true,
          'name'      => 'email',
      ));
      $fieldset->addField('phonenumber', 'label', array(
          'label'     => Mage::helper('careers')->__('Phone Number'),
          'required'  => true,
          'name'      => 'phonenumber',
      ));

      $fieldset->addField('filename', 'link', array(
          'label'     => Mage::helper('careers')->__('CV'),
          'required'  => true,
      )); 

	  
/*       $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('careers')->__('CV'),
          'required'  => false,
          'name'      => 'filename',
	  )); */
		
		$fieldset->addField('content', 'label', array(
          'name'      => 'content',
          'label'     => Mage::helper('careers')->__('Content'),
          'title'     => Mage::helper('careers')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      )); 
     
/*       $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('careers')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('careers')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('careers')->__('Disabled'),
              ),
          ),
      )); */
     
/*       $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('careers')->__('Content'),
          'title'     => Mage::helper('careers')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      )); */
     
      if ( Mage::getSingleton('adminhtml/session')->getCareersData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCareersData());
          Mage::getSingleton('adminhtml/session')->setCareersData(null);
      } elseif ( Mage::registry('careers_data') ) {
          $form->setValues(Mage::registry('careers_data')->getData());
      }
      return parent::_prepareForm();
  }
}