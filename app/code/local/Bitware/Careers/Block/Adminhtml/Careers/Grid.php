<?php

class Bitware_Careers_Block_Adminhtml_Careers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('careersGrid');
      $this->setDefaultSort('careers_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('careers/careers')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('careers_id', array(
          'header'    => Mage::helper('careers')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'careers_id',
      ));

      $this->addColumn('firstname', array(
          'header'    => Mage::helper('careers')->__('Fist Name'),
          'align'     =>'left',
          'index'     => 'firstname',
      ));
      $this->addColumn('lastname', array(
          'header'    => Mage::helper('careers')->__('Last Name'),
          'align'     =>'left',
          'index'     => 'lastname',
      ));
      $this->addColumn('email', array(
          'header'    => Mage::helper('careers')->__('E-mail Address'),
          'align'     =>'left',
          'index'     => 'email',
      ));
      $this->addColumn('phonenumber', array(
          'header'    => Mage::helper('careers')->__('Phone Number'),
          'align'     =>'left',
          'index'     => 'phonenumber',
      ));
      $this->addColumn('filename', array(
          'header'    => Mage::helper('careers')->__('CV'),
          'align'     =>'left',
          'index'     => 'filename',
      ));

	  
/*       $this->addColumn('content', array(
			'header'    => Mage::helper('careers')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      )); */
	  

      $this->addColumn('status', array(
          'header'    => Mage::helper('careers')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('careers')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('careers')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('careers')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('careers')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('careers_id');
        $this->getMassactionBlock()->setFormFieldName('careers');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('careers')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('careers')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('careers/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('careers')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('careers')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}