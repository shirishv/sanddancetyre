<?php

 /**
 * Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    FME_Quickrfq
 * @author     Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyright  Copyright 2010 © free-magentoextensions.com All right reserved
 */
 
class FME_Quickrfq_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    const XML_PATH_EMAIL_RECIPIENT  = 'quickrfq/email/recipient';
    const XML_PATH_EMAIL_SENDER     = 'quickrfq/email/sender';
    const XML_PATH_EMAIL_TEMPLATE   = 'quickrfq/email/template';
    const XML_PATH_ENABLED          = 'quickrfq/option/enable';
    const XML_PATH_UPLOAD          = 'quickrfq/upload/allow';
     const XML_PATH_SUBJECT          = 'quickrfq/email_reply/subject';
    const XML_PATH_BODY          = 'quickrfq/email_reply/body';
 
    public function getUserName()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return '';
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return trim($customer->getName());
    }

    public function getUserEmail()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return '';
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $customer->getEmail();
    }

    public function getAccountUrl()
    {
         return Mage::getUrl('quickrfq/index');
    }
    
    public function sendEmailToModerator($info_array){
		
		$m_subject = Mage::getStoreConfig(self::XML_PATH_SUBJECT);
		$m_email = Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
		//$template = Mage::getStoreConfig('contacts_email_email_template');
		$template	=	Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE);
		
		
		$sender_id = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
		$sender_email = 'owner@example.com';
		$sender_name = 'Owner';
		
		if($sender_id == 'general'){
			$sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
			$sender_name = Mage::getStoreConfig('trans_email/ident_general/name');
		
		}elseif($sender_id == 'sales'){
			$sender_email = Mage::getStoreConfig('trans_email/ident_sales/email');
			$sender_name = Mage::getStoreConfig('trans_email/ident_sales/name');
		
		}elseif($sender_id == 'support'){
			$sender_email = Mage::getStoreConfig('trans_email/ident_support/email');
			$sender_name = Mage::getStoreConfig('trans_email/ident_support/name');
		
		}elseif($sender_id == 'custom1'){
			$sender_email = Mage::getStoreConfig('trans_email/ident_custom1/email');
			$sender_name = Mage::getStoreConfig('trans_email/ident_custom1/name');
		
		}elseif($sender_id == 'custom2'){
			$sender_email = Mage::getStoreConfig('trans_email/ident_custom2/email');
			$sender_name = Mage::getStoreConfig('trans_email/ident_custom2/name');
		}

		$emailTemplate = Mage::getModel('core/email_template')->loadDefault($template);
		
	                          
		$emailTemplateVariables = array();
		
		$emailTemplateVariables['contact_name'] = $info_array['contact_name'];
		$emailTemplateVariables['phone'] = $info_array['phone'];
		$emailTemplateVariables['company'] = $info_array['company'];
		$emailTemplateVariables['project_title'] = $info_array['project_title'];
		$emailTemplateVariables['senderemail'] = $info_array['email'];
		$emailTemplateVariables['date'] = $info_array['date'];
		$emailTemplateVariables['overview'] = $info_array['overview'];
		$emailTemplateVariables['budget'] = $info_array['budget'];
		$emailTemplateVariables['servicerequested'] = $info_array['servicerequested'];
		$emailTemplateVariables['servicedatetime'] = $info_array['servicedatetime'];

		
		
		$emailTemplate->setSenderName($sender_name);
		$emailTemplate->setSenderEmail($sender_email);
		$emailTemplate->setTemplateSubject($m_subject);

		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		
		
			$emailTemplate->send($m_email,'RFQ Notification', $emailTemplateVariables);
			// $data['success'] = true;
   //      	$data['message'] = 'Message sent Successfully!';
   //      	$this->notifyCustomer($from, $fromName);
        	
		
		
		
		
		// $emailTemplate->getProcessedTemplate($info_array);
		
		
		// $emailTemplate->setSenderName($sender_name);
		// $emailTemplate->setSenderEmail($sender_email);
		// $emailTemplate->setTemplateSubject($m_subject);
		// $emailTemplate->send($m_email, 'RFQ Notification', $info_array);
		
                return $emailTemplate;
	}

}