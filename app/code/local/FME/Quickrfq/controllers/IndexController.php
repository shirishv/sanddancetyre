<?php


 /* Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Quickrfq
 * @author     Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyright  Copyright 2010 © free-magentoextensions.com All right reserved
 */
 
 
class FME_Quickrfq_IndexController extends Mage_Core_Controller_Front_Action
{   const XML_PATH_EMAIL_RECIPIENT  = 'quickrfq/email/recipient';
    const XML_PATH_EMAIL_SENDER     = 'quickrfq/email/sender';
    const XML_PATH_EMAIL_TEMPLATE   = 'quickrfq/email/template';
    const XML_PATH_ENABLED          = 'quickrfq/option/enable';
    const XML_PATH_UPLOAD          = 'quickrfq/upload/allow';
     const XML_PATH_SUBJECT          = 'quickrfq/email_reply/subject';
    const XML_PATH_BODY          = 'quickrfq/email_reply/body';
    public function preDispatch()
    {
        parent::preDispatch();

        if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) ) {
         Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Sorry This Feature is disabled temporarily'));
         $this->norouteAction();
	
	}
    }
    public function indexAction()
    {
    	
    	 $this->loadLayout();
         $this->renderLayout();
    }
     /***************************************************************
     this function validates and saves posted rfq data from frontend
     and also sends the emails to admin and the client who has
     requested the quote
    ***************************************************************/
    public function postAction()
    {
	 
	/***************************************************************
	 $rfqdata saves the posted data as an array
	***************************************************************/
	$rfqdata = $this->getRequest()->getPost();
	
	
	$params = $this->getRequest()->getParams();
	/***************************************************************
		    check whether any data posted or not
	***************************************************************/
	
	if ( $rfqdata ) {
	/***************************************************************
	    check servers date and save it as create date in database
	***************************************************************/
	   
	    $todaydate=date("Y-m-d");
	    $rfqdata['create_date']=$todaydate;
	    $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
	/***************************************************************
	    $postObject is to set all the posted data to send emails
	***************************************************************/
                $postObject = new Varien_Object();
                $postObject->setData($rfqdata);
	/***************************************************************
	    these variables are set default value as false
	    further they will be used as to check which required fields
	    are not validating
	***************************************************************/
           $nameerror = false;
		$hideiterror= false;
		$emailerror = false;
		$overviewerror = false;
		$captchaerror = false;
		$servicerequestedError = false;
		$servicedatetimeError = false;
		$phoneError = false;
	/***************************************************************
	   zend validator validates the required fields
	***************************************************************/
                if (!Zend_Validate::is(trim($rfqdata['contact_name']) , 'NotEmpty')) { 
                    $nameerror = true;
                }
/* 		if (!Zend_Validate::is(trim($rfqdata['security_code']) , 'NotEmpty')) { 
                    $captchaerror = true;
                } */

/*                 if (!Zend_Validate::is(trim($rfqdata['overview']) , 'NotEmpty')) {
                    $overviewerror = true;
                } */

                if (!Zend_Validate::is(trim($rfqdata['email']), 'EmailAddress')) {
                    $emailerror = true;
                }

                if (Zend_Validate::is(trim($rfqdata['hideit']), 'NotEmpty')) {
                    $hideiterror = true;
                }
				if(trim($rfqdata['servicerequested']) == 'Choose Your Service'){
					$servicerequestedError = true;
				}
				if(trim($rfqdata['servicerequested']) == ''){				
				//if (Zend_Validate::is(trim($rfqdata['servicedatetime']), 'NotEmpty')) {
					$servicedatetimeError = true;
				}		

				if(trim($rfqdata['phone']) == ''){				
				//if (Zend_Validate::is(trim($rfqdata['servicedatetime']), 'NotEmpty')) {
					$phoneError = true;
				}						
				
	/***************************************************************
	   if error returned by zend validator then add an error message
	***************************************************************/
/* 		 if ($captchaerror) {
		    
		    $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter verification text '));
                } */
		
		 if ($nameerror) {
		     $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter your Name'));
                }

		 if ($servicerequestedError) {
		     $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Select a Service'));
                }

		 if ($servicedatetimeError) {
		     $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Select Your Appointment Time'));
                }				
				
/* 		if ($overviewerror) {
		     $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Give a brief Overview'));
                } */
		if ($emailerror) {
		     $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter a valid Email Address'));
                }
				
		if ($phoneError) {
		     $translate->setTranslateInline(true);
                    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter Your Phone Number'));
                }				
		/***************************************************************
		    if any error occurs then throw an exception so not to move
		    from this forward this if condition
		 ***************************************************************/

		if ($hideiterror || $nameerror  || $overviewerror  || $emailerror || $captchaerror || $servicerequestedError || $servicedatetimeError || $phoneError) { 
			throw new Exception();
		}
/* 		if (!$captchaerror && $rfqdata['security_code']!= $rfqdata['captacha_code']) {
		    
				$translate->setTranslateInline(true);
				Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Sorry The Security Code You Entered Was Incorrect'));
				throw new Exception();
		} */
		/***************************************************************
		    if any file is uploaded then move first to this function
		 ***************************************************************/
                 if(isset($_FILES['prd']['name']) && $_FILES['prd']['name'] != '') {
		   
                                try {
				    
				    /***************************************************************
					get allowed extensions from backend
				    (system/configuration/{quickrfq}configurations->upload restriction
				    for rfq)
				     ***************************************************************/
					$ext = array();
					$extensions = array();
						$ext = explode(",",Mage::getStoreConfig(self::XML_PATH_UPLOAD));
						
						// foreach($ext as $exten)
						// {
						//     $exten=trim($exten);
						//     $extensions[]=$exten;
						    
						// }
                            
                                        /* Starting upload */        
                                        $uploader = new Varien_File_Uploader('prd');
                                        
                                        // Any extention would work
										 $uploader->setAllowedExtensions($ext);
										// $uploader->setAllowedExtensions(
										// 	array('jpg'));
           //                              $uploader->setAllowRenameFiles(false);
                                        
                                        // Set the file upload mode 
                                        // false -> get the file directly in the specified folder
                                        // true -> get the file in the product like folders 
                                        //        (file.jpg will go in something like /media/f/i/file.jpg)
                                        //$uploader->setFilesDispersion(false);
                                                        
                                        // We set media/quickrfq as the upload dir
                                        $path = Mage::getBaseDir('media') . DS . 'quickrfq' . DS;
					
                                        $_FILES['prd']['name']='rfq'.'_'.time().'_'.$_FILES['prd']['name'];
                                        $uploader->save($path, $_FILES['prd']['name'] );
                                        
				    }
				    catch (Exception $e) {
					
				    
				     $translate->setTranslateInline(true);

				    Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Unable to upload your file. Use the specified extensions only '));
				   throw new Exception();
                      
				 }
		/***************************************************************
		    if any file is uploaded then and its name in the posted data
		    array to save files name in the database
		 ***************************************************************/	 
                        $rfqdata['prd'] = $_FILES['prd']['name'];
                        
		}
	    /***************************************************************
		if any any none required field is left empty then use a
		default text of "Not Mentioned" in it instead of empty field
		in database.
	     ***************************************************************/
		 foreach($rfqdata as $key => $value)
	    {
		if($key=='hideit')
		{break;}
		if($value == null)
		{
		    $rfqdata[$key]="Not Mentioned";
		}
	    }
	    /***************************************************************
		Now after all workings save the data to DB
	     ***************************************************************/
		    $model = Mage::getModel('quickrfq/quickrfq');
		    $model->setData($rfqdata)->setId($this->getRequest()->getParam('id'));
		    try { 
                            $model->save();
			    /***************************************************************
				Send all data to the email address saved at backend
			    (system/configuration/{quickrfq}configurations->email setup options
			    for the rfq reciever)
			     ***************************************************************/
				    
				   /* $mailTemplate = Mage::getModel('core/email_template');
				     // @var $mailTemplate Mage_Core_Model_Email_Template 
				    $mailTemplate->setDesignConfig(array('area' => 'frontend'))
					->setReplyTo($rfqdata['email'])
					->sendTransactional(
					    Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
					    Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
					    Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
					    null,
					    array('data' => $postObject)
					);
					*/
						
						
						
					$emailTemplate = Mage::helper('quickrfq')->sendEmailToModerator($rfqdata);	
					
		    
			//	    if (!$emailTemplate->getSentSuccess()) {
			//		throw new Exception();
			//	    }
			    /****************************************************************
			    Send an email to the client also and get its email subject and
			    the body from backend
			    (system/configuration/{quickrfq}configurations->Reply to customer)
			    *****************************************************************/    
				    $mail = new Zend_Mail();
				    $subject=Mage::getStoreConfig(self::XML_PATH_SUBJECT);
				    $subject=trim($subject);
				    if(empty($subject))
				    {
					$subject="Thank You for Requesting A Quote";
				    }
				    $body=Mage::getStoreConfig(self::XML_PATH_BODY);
				    $body=trim($body);
				    if(empty($body))
				    {
					$body="Your request has been forwarded to the concerning department and they will be in touch shortly. 


Thank you once again for visiting our site and contacting us for a Quote. 



Regards ";
				    }
				    $mail->setBodyText($body);
				    $mail->setFrom(Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT));
				    $mail->addTo($rfqdata['email']);
				    $mail->setSubject($subject);
					
				    try {
						$mail->send();					
						//$to = $rfqdata['email'].",james@sdtyres.com";					
						//$to = $rfqdata['email'];					
						//$subject = "Thank You Contacting to us.";


						$to = $rfqdata['email'];
						
						$subject = 'Thank You Contacting to us.';
						$from = 'james@sdtyres.com';
						//$from = 'wasim@bitwaretechnologies.com';

						$headers = "From: Customer Service <" . strip_tags($from) . ">\r\n";
						$headers .= "Reply-To: ". strip_tags($from) . "\r\n";
						//$headers .= "CC: susan@example.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
					
						$message = "<div><p>Hello</p> 
						<p style='margin-top:15px;'>Thank you for your booking with Sand Dance tyres. <br/>If you have any questions please feel free to contact our customer service team on 800 222 111 </p><br/>
						<div><p>Regards</p>
						<p style='margin-top:-15px;'><a href='".Mage::getBaseUrl()."' alt='Sand dance' style='color:#1e7ec8' target='_blank'><img src='".Mage::getBaseUrl()."skin/frontend/bitware/sanddance/images/sanddance.png' alt='Sand Dance' class='large'></a></p>
						<p>Customer service team</p></div>
						</div>";		
						
						//$from = "from@fromaddress.com";					
							
						//$headers .= "MIME-Version: 1.0\r\n";
						//$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
						
						//$headers = "From:james@sdtyres.com";
						
						
						
						
						if (mail($to, $subject, $message, $headers)) {						
							echo("Your message has been sent successfully");	

							$name = $rfqdata['contact_name'];
							//echo '<br/>name:'.$name.'<br/>';
							$email = $rfqdata['email'];
							//echo '<br/>email:'.$email.'<br/>';
							$telephone = $rfqdata['phone'];
							//echo '<br/>telephone:'.$telephone.'<br/>';
							$service_requested = $rfqdata['servicerequested'];
							//echo '<br/>service_requested:'.$service_requested.'<br/>';
							$service_details = $rfqdata['servicedatetime'];
							//echo '<br/>service_details:'.$service_details.'<br/>';
							
							
							//$toNew = 'satish@bitwaretechnologies.com';
							$toNew = 'james@sdtyres.com';
							//$toNew = 'wasim@bitwaretechnologies.com';
							$subjectNew = 'Thank You!';
							
							$fromEmail = 'james@sdtyres.com';
							//$fromEmail = 'wasim@bitwaretechnologies.com';

							$header = "From: Customer Service <" . strip_tags($fromEmail) . ">\r\n";
							$header .= "MIME-Version: 1.0\r\n";
							$header .= "Content-Type: text/html; charset=UTF-8\r\n";
							

							$messageNew = '<div style="font:11px/1.35em Verdana,Arial,Helvetica,sans-serif">
        <span class="HOEnZb"><font color="#888888">
                </font></span><span class="HOEnZb"><font color="#888888">
            </font></span><table style="margin-top:10px;font:11px/1.35em Verdana,Arial,Helvetica,sans-serif;margin-bottom:10px" width="98%" cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                    <td valign="top" align="center"><span>
                        A new request for appointment has been Submitted with the following Details. The Details are as follows.
                        
                        
                          <table width="650" cellspacing="0" cellpadding="0" border="0">
                              <tbody><tr>
                                    <td valign="top">
                                     <p><a href="'.Mage::getBaseUrl().'" alt="Sand dance"><img src="http://www.sanddancetyre.com/media/sanddance.png" alt="Sand Dance" class="CToWUd" border="0"></a></p></td>
                                </tr>
                           </tbody></table>
                           
                          </span><table width="650" cellspacing="0" cellpadding="0" border="0">
                                   
                             
                              <tbody><tr>
                                   <td style="text-align:left;width:30%;font-weight:bold" align="center">Name:</td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center">'.$name.'</td>
                             </tr>
                          </tbody></table>
                           <table width="650" cellspacing="0" cellpadding="0" border="0">
                          
                              <tbody><tr>
                                   <td style="text-align:left;width:30%;font-weight:bold" align="center">E-mail: </td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center"><a href="'.$email.'" target="_blank">'.$email.'</a></td>
                             </tr>
                               </tbody></table>

                           <table width="650" cellspacing="0" cellpadding="0" border="0">
                              <tbody><tr>
                                   <td style="text-align:left;width:30%;font-weight:bold" align="center">Telephone: </td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center"> '.$telephone.'</td>
                             </tr>
                               <tr>
                                   <td style="text-align:left;width:30%;font-weight:bold" align="center">Service Requested:</td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center"> '.$service_requested.'</td>
                             </tr>
                                </tbody></table>
                           <table width="650" cellspacing="0" cellpadding="0" border="0">
                               <tbody><tr>
                                   <td style="text-align:left;width:30%;font-weight:bold" align="center">Service Details:</td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center"> <span class="aBn" data-term="goog_447731743" tabindex="0"><span class="aQJ">'.$service_details.'</td>
                             </tr>
                            </tbody></table>

                           <table width="650" cellspacing="0" cellpadding="0" border="0">
                              <tbody><tr>
                                   <td style="text-align:left;width:30%" align="center">&nbsp;</td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center">&nbsp;</td>
                             </tr>
                            </tbody></table>
                            <span class="HOEnZb"><font color="#888888">
                            </font></span><table width="650" cellspacing="0" cellpadding="0" border="0">
                              <tbody><tr>
                                   <td style="text-align:left;width:30%;font-weight:bold" align="center">Regards,<br/>Customer service team</td>
                                   <td style="padding-left:5px;text-align:left;width:70%" align="center">&nbsp; </td>
                             </tr></tbody></table><span class="HOEnZb"><font color="#888888">
                          
                    </font></span></td></tr></tbody></table><div class="yj6qo ajU"><div id=":1sa" class="ajR" role="button" tabindex="0" data-tooltip="Show trimmed content" aria-label="Show trimmed content"><img class="ajT" src="//ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif"></div></div><span class="HOEnZb adL"><font color="#888888">
</font></span></div>';
							
							if(mail($toNew, $subjectNew, $messageNew, $header)) {						
								echo("Your message has been sent successfully");						
							} 
							else 
							{						
								echo("Sorry, your message could not be sent");					
							}
						} 
						else 
						{						
							echo("Sorry, your message could not be sent");					
						}	

						/* Email send to admin */
						
						//$to1 = 'james@sdtyres.com';
						
	

						//exit;
				    }        
				    catch(Exception $e) {
					Mage::getSingleton('core/session')->addError('Unable to send email to your account ');
			     
				    }
			    $translate->setTranslateInline(true);
			    Mage::getSingleton('core/session')->addSuccess(Mage::helper('quickrfq')->__('Your Request was submitted and will be responded as soon as possible. Thank you for contacting us.'));
			    $this->_redirect('*/*/');
			    return;
			}
		    catch (Exception $e)
			{
			    
			 $translate->setTranslateInline(true);
	
			Mage::getSingleton('core/session')->addError($e->getMessage());
			 $this->_redirect('*/*/');
			return;
			 }
		
	 
	}
	    
	catch (Exception $e) {
	  
                $translate->setTranslateInline(true);
		Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Unable to submit your request. Please, try again later'));
		 $this->_redirect('*/*/');
                return;
            }

        }
	else
	{
           
               $this->_redirect('*/*/');

        }
      
    }
    
}?>