<?php
/**
 *
 * @category    Kamtech
 * @package     Kamtech_Productselector
 * @copyright   Copyright (c) 2010 Kam Technology
 */
class Kamtech_Ts_TiresearchController extends Mage_Core_Controller_Front_Action {        

    public function indexAction() {

        //echo 'Index!';
		//place holder for index actions

    }


    public function paramsAction(){
    
	   // echo '<dl>';            
	    foreach($this->getRequest()->getParams() as $key=>$value) {
		//echo '<dt><strong>Param: </strong>'.$key.'</dt>';
		//echo '<dt><strong>Value: </strong>'.$value.'</dt>';
	    }
	   // echo '</dl>';    
    }
    
   
    
    public function findwidthAction(){
		$widthattribute_code="width";
        $resource = Mage::getSingleton ( 'core/resource' );
		$readonce = $resource->getConnection ( 'core_read' );
		$table1=$resource->getTableName ( 'eav_attribute' );
		$table2=$resource->getTableName ( 'catalog_product_entity_varchar' );

		$widthquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$widthattribute_code."'");
		$row = $widthquery->fetch ();
		$width_attributeid=$row['attribute_id'];
	
				$genquery = $readonce->query("select distinct value from ".$table2." where  attribute_id=".$width_attributeid ." order by value ");
        
		echo '<select name="width"><option>Select Width</option>';
        
        while($rowz = $genquery->fetch ()){
        
        echo '<option value>'.$rowz['value'].'</option>';
               
        }
        echo '</select>';
    
        }
        
	   
                
   public function findratioAction(){
    // echo '<dt><strong>findmodelAction: Enter </strong> </dt>';
        foreach($this->getRequest()->getParams() as $width=>$value1) {
            //echo '<dt><strong>Param: </strong>'.$main_cat.'</dt>';
            //echo '<dt><strong>Value: </strong>'.$value.'</dt>';
        }
        if($value1=="-1")
        {
        //echo '<dt><strong>Param value: </strong>'.$value1.'</dt>';
         echo '<select name="ratio"><option id=-1 value="">Select Profile</option></select>';
         return;
        }
		$widthattribute_code="width";
       $ratioattribute_code="ratio";
       $resource = Mage::getSingleton ( 'core/resource' );
       $readonce = $resource->getConnection ( 'core_read' );
       $table1=$resource->getTableName ( 'eav_attribute' );
       $table2=$resource->getTableName ( 'catalog_product_entity_varchar' );

	   $widthquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$widthattribute_code."'");
		$row = $widthquery->fetch ();
		$width_attributeid=$row['attribute_id'];
	

       $ratioquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$ratioattribute_code."'");
       $row = $ratioquery->fetch ();
       $ratio_attributeid=$row['attribute_id'];
    	//echo '<dt><strong>Model Attribute: </strong>'.$model_attributeid.'</dt>';

        $queryString="SELECT distinct value from ".$table2." where attribute_id=".$ratio_attributeid ." and entity_id in (select entity_id from ".$table2." where attribute_id=".$width_attributeid ." and value like '%".$value1."%') order by value";
	
	   $genquery2 = $readonce->query("SELECT distinct value from ".$table2." where attribute_id=".$ratio_attributeid ." and entity_id in (select entity_id from ".$table2." where attribute_id=".$width_attributeid ." and value like '%".$value1."%') order by value");

		$fname="getResponseToDiv('finddiameter?ratio='+this.options[this.selectedIndex].value+'&width='+tire.width.value,'diameterdiv',this.options[this.selectedIndex].value)";
		//echo '<dt><strong>fname: </strong>'.$fname.'</dt>';
        echo '<select id=ratio name=ratio onChange='.$fname.' ><option id=-1 value="">Select Profile</option>';  
         
        $allzratio='';
       	while($row = $genquery2->fetch ())
       	{
			$allzratio=$allzratio.$row['value'].',';
		}
		$rzinput=explode(',',$allzratio);
		$rzresult = array_unique($rzinput);
		sort($rzresult);
		for($rz=1;$rz<count($rzresult); $rz++){
		echo '<option value='.'"'.$rzresult[$rz].'"'.' >'.$rzresult[$rz].' </option>';
          }
	    //echo '<option value='.$row['value'].' >'.$row['value'].'</option>';
        
            echo '</select>';  

   }   
   
 // Find Car Model
   public function findcar_modelAction(){

        foreach($this->getRequest()->getParams() as $width=>$value1) {
       
        }
        if($value1=="-1")
        {
        //echo '<dt><strong>Param value: </strong>'.$value1.'</dt>';
         echo '<select name="ratio"><option id=-1 value="">Select Profile</option></select>';
         return;
        }
		$widthattribute_code="car_make";
       $ratioattribute_code="car_model";
       $resource = Mage::getSingleton ( 'core/resource' );
       $readonce = $resource->getConnection ( 'core_read' );
       $table1=$resource->getTableName ( 'eav_attribute' );
       $table2=$resource->getTableName ( 'catalog_product_entity_varchar' );

	   $widthquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$widthattribute_code."'");
		$row = $widthquery->fetch ();
		$width_attributeid=$row['attribute_id'];
	

       $ratioquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$ratioattribute_code."'");
       $row = $ratioquery->fetch ();
       $ratio_attributeid=$row['attribute_id'];
    	//echo '<dt><strong>Model Attribute: </strong>'.$model_attributeid.'</dt>';

        $queryString="SELECT distinct value from ".$table2." where attribute_id=".$ratio_attributeid ." and entity_id in (select entity_id from ".$table2." where attribute_id=".$width_attributeid ." and value like '%".$value1."%') order by value";
	
	   $genquery2 = $readonce->query("SELECT distinct value from ".$table2." where attribute_id=".$ratio_attributeid ." and entity_id in (select entity_id from ".$table2." where attribute_id=".$width_attributeid ." and value like '%".$value1."%') order by value");

		$fname="getResponseToDiv('findcar_year?car_model='+this.options[this.selectedIndex].value+'&car_make='+tire.car_make.value,'car_yeardiv',this.options[this.selectedIndex].value)";
		//echo '<dt><strong>fname: </strong>'.$fname.'</dt>';
        echo '<select id=car_model name=car_model onChange='.$fname.' ><option id=-1 value="">Select Model</option>';  
         
        $allzratio='';
       	while($row = $genquery2->fetch ())
       	{
			$allzratio=$allzratio.$row['value'].',';
		}
		$rzinput=explode(',',$allzratio);
		$rzresult = array_unique($rzinput);
		sort($rzresult);
		for($rz=1;$rz<count($rzresult); $rz++){
		echo '<option value='.'"'.$rzresult[$rz].'"'.' >'.$rzresult[$rz].' </option>';
          }
	    //echo '<option value='.$row['value'].' >'.$row['value'].'</option>';
        
            echo '</select>';  

   }    
   
   public function finddiameterAction(){
	   $request = $this->getRequest();
	   $value2=$request->getParam('ratio');
	   $value1=$request->getParam('width');
        

        if($value2=="-1")
        {
        //echo '<dt><strong>Param value: </strong>'.$value.'</dt>';
         echo '<select name="diameter"><option id=-1 value="">Select Rim Size</option></select>';
         return;
        }
       $widthattribute_code="width";
       $ratioattribute_code="ratio";
       $diameter_attribute_code="diameter";
       $resource = Mage::getSingleton ( 'core/resource' );
       $readonce = $resource->getConnection ( 'core_read' );
       $table1=$resource->getTableName ( 'eav_attribute' );
       $table2=$resource->getTableName ( 'catalog_product_entity_varchar' );

	   $widthquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$widthattribute_code."'");
		$row = $widthquery->fetch ();
		$width_attributeid=$row['attribute_id'];
	

       $ratioquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$ratioattribute_code."'");
       $row = $ratioquery->fetch ();
       $ratio_attributeid=$row['attribute_id'];


       $diameterquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$diameter_attribute_code."'");
       $row = $diameterquery->fetch ();
       $diameter_attributeid=$row['attribute_id'];

	   $queryString="select distinct value from ".$table2." where attribute_id=".$diameter_attributeid." and entity_id in(select entity_id from ".$table2." where  attribute_id=".$width_attributeid ." and value like '%".$value1."%' and entity_id in(select entity_id from ".$table2." where  attribute_id=".$ratio_attributeid ." and value like '%".$value2."%')) order by value";

	   //echo '<dt><strong>Query: </strong>'.$queryString.'</dt>';

       $genquery2 = $readonce->query("select distinct value from ".$table2." where attribute_id=".$diameter_attributeid." and entity_id in(select entity_id from ".$table2." where  attribute_id=".$width_attributeid ." and value like '%".$value1."%' and entity_id in(select entity_id from ".$table2." where  attribute_id=".$ratio_attributeid ." and value like '%".$value2."%')) order by value");
 //      $fname="getResponseToDiv('findmanufacturer?diameter='+this.options[this.selectedIndex].value+'&ratio='+tire.ratio.value+'&width='+tire.width.value,'manufacturerdiv',this.options[this.selectedIndex].value)";
       //echo '<dt><strong>fname: </strong>'.$fname.'</dt>';
        echo '<select id=diameter name="diameter" ><option id=-1 value="">Select Rim Size</option>';
		$allzdiameter='';
       	while($row = $genquery2->fetch ())
       	{
            $allzdiameter=$allzdiameter.$row['value'].',';
		}
		$dzinput=explode(',',$allzdiameter);
		$dzresult = array_unique($dzinput);
		sort($dzresult);
		for($dz=1;$dz<count($dzresult); $dz++){
		echo '<option value='.'"'.$dzresult[$dz].'"'.' >'.$dzresult[$dz].' </option>';
          }
	    //echo '<option value='.$row['value'].'>'.$row['value'].'</option>';
		
        echo '</select>';  

	}
	
   public function findcar_yearAction(){
	   $request = $this->getRequest();
	   $value2=$request->getParam('car_model');
	   $value1=$request->getParam('car_make');
        

        if($value2=="-1")
        {
        //echo '<dt><strong>Param value: </strong>'.$value.'</dt>';
         echo '<select name="car_year"><option id=-1 value="">Select Year</option></select>';
         return;
        }
       $widthattribute_code="car_make";
       $ratioattribute_code="car_model";
       $diameter_attribute_code="car_year";
       $resource = Mage::getSingleton ( 'core/resource' );
       $readonce = $resource->getConnection ( 'core_read' );
       $table1=$resource->getTableName ( 'eav_attribute' );
       $table2=$resource->getTableName ( 'catalog_product_entity_varchar' );

	   $widthquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$widthattribute_code."'");
		$row = $widthquery->fetch ();
		$width_attributeid=$row['attribute_id'];
	

       $ratioquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$ratioattribute_code."'");
       $row = $ratioquery->fetch ();
       $ratio_attributeid=$row['attribute_id'];


       $diameterquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$diameter_attribute_code."'");
       $row = $diameterquery->fetch ();
       $diameter_attributeid=$row['attribute_id'];

	   $queryString="select distinct value from ".$table2." where attribute_id=".$diameter_attributeid." and entity_id in(select entity_id from ".$table2." where  attribute_id=".$width_attributeid ." and value like '%".$value1."%' and entity_id in(select entity_id from ".$table2." where  attribute_id=".$ratio_attributeid ." and value like '%".$value2."%')) order by value";

	   //echo '<dt><strong>Query: </strong>'.$queryString.'</dt>';

       $genquery2 = $readonce->query("select distinct value from ".$table2." where attribute_id=".$diameter_attributeid." and entity_id in(select entity_id from ".$table2." where  attribute_id=".$width_attributeid ." and value like '%".$value1."%' and entity_id in(select entity_id from ".$table2." where  attribute_id=".$ratio_attributeid ." and value like '%".$value2."%')) order by value");
      // $fname="getResponseToDiv('findmanufacturer?diameter='+this.options[this.selectedIndex].value+'&ratio='+tire.ratio.value+'&width='+tire.width.value,'manufacturerdiv',this.options[this.selectedIndex].value)";
       //echo '<dt><strong>fname: </strong>'.$fname.'</dt>';
        echo '<select id=car_year name="car_year" ><option id=-1 value="">Select Year</option>';
		$allzdiameter='';
       	while($row = $genquery2->fetch ())
       	{
            $allzdiameter=$allzdiameter.$row['value'].',';
		}
		$dzinput=explode(',',$allzdiameter);
		$dzresult = array_unique($dzinput);
		sort($dzresult);
		for($dz=1;$dz<count($dzresult); $dz++){
		echo '<option value='.'"'.$dzresult[$dz].'"'.' >'.$dzresult[$dz].' </option>';
          }
	    //echo '<option value='.$row['value'].'>'.$row['value'].'</option>';
		
        echo '</select>';  

	}	


	public function findmanufacturerAction()
		{
		
		
       $request = $this->getRequest();
       //echo '<dt><strong>Param1-year: </strong>'.$request->getParam('year').'</dt>';
       $value3=$request->getParam('diameter');
       $value2=$request->getParam('ratio');
	   $value1=$request->getParam('width');
       
        if($value3=="-1")
        {
        //echo '<dt><strong>Param value: </strong>'.$value.'</dt>';
         echo '<select name="manufacturer"><option id=-1 value="">Select Manufacturer</option></select>';
         return;
        }
	   $widthattribute_code="width";
       $ratioattribute_code="ratio";
       $diameter_attribute_code="diameter";
       $manufacturer_attribute_code="manufacture";
       $resource = Mage::getSingleton ( 'core/resource' );
       $readonce = $resource->getConnection ( 'core_read' );
       $table1=$resource->getTableName ( 'eav_attribute' );
       $table2=$resource->getTableName ( 'catalog_product_entity_varchar' );

	   $widthquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$widthattribute_code."'");
		$row = $widthquery->fetch ();
		$width_attributeid=$row['attribute_id'];
	
       $ratioquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$ratioattribute_code."'");
       $row = $ratioquery->fetch ();
       $ratio_attributeid=$row['attribute_id'];


       $diameterquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$diameter_attribute_code."'");
       $row = $diameterquery->fetch ();
       $diameter_attributeid=$row['attribute_id'];

       $manufacturerquery = $readonce->query("select attribute_id from ".$table1." where attribute_code='".$manufacturer_attribute_code."'");
       $row = $manufacturerquery->fetch ();
       $manufacturer_attributeid=$row['attribute_id'];

       
	   $queryString="SELECT distinct value from ".$table2." where attribute_id=".$manufacturer_attributeid ." and entity_id in (select entity_id from ".$table2." where entity_id in (select entity_id from ".$table2." where attribute_id=".$diameter_attributeid." and value like '%".$value3."%' and entity_id IN (SELECT entity_id FROM ".$table2." WHERE attribute_id=".$width_attributeid ." and value like '%".$value1."%'))and attribute_id=".$ratio_attributeid ." and value like '%".$value2."%') order by value";

       //echo '<dt><strong>Query: </strong>'.$queryString.'</dt>';
       $genquery2 = $readonce->query("SELECT distinct value from ".$table2." where attribute_id=".$manufacturer_attributeid ." and entity_id in (select entity_id from ".$table2." where entity_id in (select entity_id from ".$table2." where attribute_id=".$diameter_attributeid." and value like '%".$value3."%' and entity_id IN (SELECT entity_id FROM ".$table2." WHERE attribute_id=".$width_attributeid ." and value like '%".$value1."%'))and attribute_id=".$ratio_attributeid ." and value like '%".$value2."%') order by value");
       

        echo '<select id=manufacturer name="manufacturer"><option id=-1 value="">Select Manufacturer</option>';  
		$allzmanu='';
       	while($row = $genquery2->fetch ())
       	{
			$allzmanu=$allzmanu.$row['value'].',';
		}
		$manzinput=explode(',',$allzmanu);
		$manzresult = array_unique($manzinput);
		sort($manzresult);
		for($manz=1;$manz<count($manzresult); $manz++){
		echo '<option value='.'"'.$manzresult[$manz].'"'.' >'.$manzresult[$manz].' </option>';
          }
	     //echo '<option value='.$row['value'].'>'.$row['value'].'</option>';
		
        echo '</select>';  

	}
} 
