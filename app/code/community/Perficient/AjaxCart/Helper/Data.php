<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUMITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUMITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   GIT:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */



/**
 * Perficient_AjaxCart_Helper_Data
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   Release:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */
class Perficient_AjaxCart_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_PATH_ENABLED = 'perficient_ajaxcart/general/is_enabled';
    const RELATED_PRODUCTS = 1;
    const UP_SELLS = 2;
    const CROSS_SELLS = 3;
    const XML_PATH_AUTO_HIDE_POPUP = 'perficient_ajaxcart/frontend/auto_hide_popup';
    const XML_PATH_RULE_PRODUCTS_BLOCK = 'perficient_ajaxcart/frontend/show_rule_products_block';

    public function setIsModuleEnabled($value)
    {
        Mage::getModel('core/config')->saveConfig(self::XML_PATH_ENABLED, $value);
    }

    /**
     * Enable/Disable module
     *
     * @return int
     */
    public function getIsEnabled()
    {
        return (int) Mage::getStoreConfig(self::XML_PATH_ENABLED);
    }

    /**
     * Retrieve product block type
     *
     * @return string
     */
    public function getRuleProductBlockType()
    {
        return Mage::getStoreConfig(self::XML_PATH_RULE_PRODUCTS_BLOCK);
    }

    /**
     * Retrieve auto hide popup flag
     *
     * @return string
     */
    public function getIsAutoHidePopup()
    {
        return Mage::getStoreConfig(self::XML_PATH_AUTO_HIDE_POPUP);
    }

    public function getAdditionalProductBlock($blockType)
    {
        switch ($blockType) {
            case self::RELATED_PRODUCTS:
                $additionalBlock = '';
                break;
            case self::UP_SELLS:
                $additionalBlock = '';
                break;
            case self::CROSS_SELLS:
            default:
                $additionalBlock = Mage::app()->getLayout()->createBlock(
                    'enterprise_targetrule/checkout_cart_crosssell',
                    'crosssell'
                )
                    ->setTemplate('targetrule/checkout/cart/popup/crosssell.phtml')->toHtml();
                break;
        }
        return $additionalBlock;
    }

}