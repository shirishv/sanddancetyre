<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUMITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUMITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   GIT:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */

require_once(
    Mage::getModuleDir('controllers', 'Mage_Checkout')
    . DS . 'CartController.php'
);

/**
 * Perficient_AjaxCart_CartController
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   Release:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */
class Perficient_AjaxCart_CartController extends Mage_Checkout_CartController
{

    const XML_PATH_ENABLED = 'perficient_ajaxcart/general/is_enabled';


    /**
     * Pre Dispatch Method
     *
     * @return void
     */
    public function preDispatch()
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::preDispatch();
        }
        parent::preDispatch();

        $response = array();

        if (!Mage::getStoreConfigFlag(self::XML_PATH_ENABLED)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            $response["message"] = $this->__(
                'Cannot add/delete product from shopping cart.'
                . ' Please Enable the AjaxCart extension.'
            );
            return;
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }//end preDispatch()


    /**
     * Add product to shopping cart action
     *
     * @return void
     */
    public function addAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::addAction();
        }
        $response = array();

        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter        = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $message             = $this->__(
                    'Cannot add the item to shopping cart.'
                );
                $response["message"] = $message;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent(
                'checkout_cart_add_product_complete',
                array(
                 'product'  => $product,
                 'request'  => $this->getRequest(),
                 'response' => $this->getResponse(),
                )
            );


            if (!$cart->getQuote()->getHasError()) {
                $message             = $this->__(
                    '%s was added to your shopping cart.',
                    Mage::helper('core')->htmlEscape($product->getName())
                );
                $response["message"] = $message;

                //Get Layout update content
                $layout = $this->getLayout();
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_in')
                        ->load();
                } else {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_out')
                        ->load();
                }
                $layout->generateXml()->generateBlocks();
                $versionInfo = Mage::getVersionInfo();
                $version     = $versionInfo['major']
                    . '.' . $versionInfo['minor'];
                if ($version == '1.9') {
                    $header = $layout->getBlock('minicart_head')->toHtml();
                } else {
                    $header = $layout->getBlock('header')->toHtml();
                }

                if (!Mage::getConfig()
                        ->getNode('modules/Enterprise_PageCache/active')
                    && $versionInfo != '1.9') {
                    $response["header"] = preg_replace(
                        "#<div class=\"nav-container\">(.*?)</div>#is",
                        "", trim($header)
                    );
                } else {
                    $response["header"] = trim($header);
                }
                if ($blockType = Mage::helper('perficient_ajaxcart')
                    ->getRuleProductBlockType()) {
                    $response["additional"] =Mage::helper('perficient_ajaxcart')
                        ->getAdditionalProductBlock($blockType);
                }
            }
        } catch (Mage_Core_Exception $e) {
            $message             = $e->getMessage();
            $response["message"] = $message;
        } catch (Exception $e) {
            $message             = $this->__(
                'Cannot add the item to shopping cart.'
            );
            $response["message"] = $message;
            // log exception to exceptions log
            $error = sprintf(
                'Exception message: %s%sTrace: %s', $e->getMessage(), "\n",
                $e->getTraceAsString()
            );
            $file  = Mage::getStoreConfig(self::XML_PATH_LOG_EXCEPTION_FILE);
            Mage::log($error, Zend_Log::DEBUG, $file);
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }//end addAction()


    /**
     * Update product configuration for a cart item
     *
     * @return void
     */
    public function updateItemOptionsAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::updateItemOptionsAction();
        }
        $response = array();
        $cart     = $this->_getCart();
        $id       = (int) $this->getRequest()->getParam('id');
        $params   = $this->getRequest()->getParams();

        if (!isset($params['options'])) {
            $params['options'] = array();
        }
        try {
            if (isset($params['qty'])) {
                $filter        = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                $message             = $this->__('Quote item is not found.');
                $response["message"] = $message;
            }

            $item = $cart->updateItem($id, new Varien_Object($params));
            if (is_string($item)) {
                $message             = $item;
                $response["message"] = $message;
            }
            if ($item->getHasError()) {
                $message             = $item->getMessage();
                $response["message"] = $message;
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            Mage::dispatchEvent(
                'checkout_cart_update_item_complete',
                array(
                 'item'     => $item,
                 'request'  => $this->getRequest(),
                 'response' => $this->getResponse(),
                )
            );
            if (!$cart->getQuote()->getHasError()) {
                $message             = $this->__(
                    '%s was updated in your shopping cart.',
                    Mage::helper('core')->htmlEscape(
                        $item->getProduct()->getName()
                    )
                );
                $response["message"] = $message;
                $response["item_id"] = $item->getId();

                //Get Layout update content
                $layout = $this->getLayout();
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_in')
                        ->load();
                } else {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_out')
                        ->load();
                }
                $layout->generateXml()->generateBlocks();
                $versionInfo = Mage::getVersionInfo();
                $version     = $versionInfo['major']
                    . '.' . $versionInfo['minor'];
                if ($version == '1.9') {
                    $header = $layout->getBlock('minicart_head')->toHtml();
                } else {
                    $header = $layout->getBlock('header')->toHtml();
                }
                if (!Mage::getConfig()
                        ->getNode('modules/Enterprise_PageCache/active')
                    && $version != '1.9') {
                    $response["header"] = preg_replace(
                        "#<div class=\"nav-container\">(.*?)</div>#is",
                        "", trim($header)
                    );
                } else {
                    $response["header"] = trim($header);
                }
            }
        } catch (Mage_Core_Exception $e) {
            $message             = $e->getMessage();
            $response["message"] = $message;
        } catch (Exception $e) {
            $message             = $this->__('Cannot update the item.');
            $response["message"] = $message;
            Mage::logException($e);
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }//end updateItemOptionsAction()


    /**
     * Delete shopping cart item action
     *
     * @return void
     */
    public function deleteAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::deleteAction();
        }
        $response = array();
        $id       = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                    ->save();
                $message             = $this->__(
                    'Item was removed from your shopping cart.'
                );
                $response["message"] = $message;

                //Get Layout update content
                $layout = $this->getLayout();
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_in')
                        ->addHandle('checkout_cart_index')
                        ->load();
                } else {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_out')
                        ->addHandle('checkout_cart_index')
                        ->load();
                }
                $layout->generateXml()->generateBlocks();
                $versionInfo = Mage::getVersionInfo();
                $version     = $versionInfo['major']
                    . '.' . $versionInfo['minor'];
                if ($version == '1.9') {
                    $header = $layout->getBlock('minicart_head')->toHtml();
                } else {
                    $header = $layout->getBlock('header')->toHtml();
                }
                $content = $layout->getBlock('content')->toHtml();
                if (!Mage::getConfig()
                        ->getNode('modules/Enterprise_PageCache/active')
                    && $version != '1.9') {
                    $response["header"] = preg_replace(
                        "#<div class=\"nav-container\">(.*?)</div>#is",
                        "", trim($header)
                    );
                } else {
                    $response["header"] = trim($header);
                }
                $response["content"] = trim($content);
            } catch (Exception $e) {
                $message             = $this->__('Cannot remove the item.');
                $response["message"] = $message;
                Mage::logException($e);
            }
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }//end deleteAction()


    /**
     * Update shopping cart data action
     *
     * @return void
     */
    public function updatePostAction()
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::updatePostAction();
        }
        $response = array();
        try {
            $cart         = $this->_getCart();
            $cartData     = $this->getRequest()->getParam('cart');
            $updateAction = (string) $this->getRequest()
                ->getParam('update_cart_action');
            if (isset($updateAction) && $updateAction == 'empty_cart') {

                $cart->truncate();
                $cart->save();
                $this->_getSession()->setCartWasUpdated(true);
                $message             = $this->__('Shopping cart is empty.');
                $response["message"] = $message;
                //Get Layout update content
                $layout = $this->getLayout();
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_in')
                        ->addHandle('checkout_cart_index')
                        ->load();
                } else {
                    $layout->getUpdate()
                        ->addHandle('default')
                        ->addHandle('customer_logged_out')
                        ->addHandle('checkout_cart_index')
                        ->load();
                }
                $layout->generateXml()->generateBlocks();
                $versionInfo = Mage::getVersionInfo();
                $version     = $versionInfo['major']
                    . '.' . $versionInfo['minor'];
                if ($version == '1.9') {
                    $header = $layout->getBlock('minicart_head')->toHtml();
                } else {
                    $header = $layout->getBlock('header')->toHtml();
                }
                $content = $layout->getBlock('content')->toHtml();
                if (!Mage::getConfig()
                        ->getNode('modules/Enterprise_PageCache/active')
                    && $version != '1.9') {
                    $response["header"] = preg_replace(
                        "#<div class=\"nav-container\">(.*?)</div>#is",
                        "", trim($header)
                    );
                } else {
                    $response["header"] = trim($header);
                }
                $response["content"] = trim($content);
            } else {
                if (is_array($cartData)) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array(
                         'locale' => Mage::app()->getLocale()->getLocaleCode(),
                        )
                    );
                    foreach ($cartData as $index => $data) {
                        if (isset($data['qty'])) {
                            $cartData[$index]['qty'] = $filter->filter(
                                trim($data['qty'])
                            );
                        }
                    }
                    $cart = $this->_getCart();
                    if (!$cart->getCustomerSession()->getCustomer()->getId()
                        && $cart->getQuote()->getCustomerId()) {
                        $cart->getQuote()->setCustomerId(null);
                    }

                    $cartData = $cart->suggestItemsQty($cartData);
                    $cart->updateItems($cartData)
                        ->save();
                    $message             = $this->__(
                        'Shopping cart was updated.'
                    );
                    $response["message"] = $message;

                    //Get Layout update content
                    $layout = $this->getLayout();
                    if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                        $layout->getUpdate()
                            ->addHandle('default')
                            ->addHandle('customer_logged_in')
                            ->addHandle('checkout_cart_index')
                            ->load();
                    } else {
                        $layout->getUpdate()
                            ->addHandle('default')
                            ->addHandle('customer_logged_out')
                            ->addHandle('checkout_cart_index')
                            ->load();
                    }
                    $layout->generateXml()->generateBlocks();
                    $versionInfo = Mage::getVersionInfo();
                    $version     = $versionInfo['major']
                        . '.' . $versionInfo['minor'];
                    if ($version == '1.9') {
                        $header = $layout->getBlock('minicart_head')->toHtml();
                    } else {
                        $header = $layout->getBlock('header')->toHtml();
                    }
                    $content = $layout->getBlock('content')->toHtml();
                    if (!Mage::getConfig()
                            ->getNode('modules/Enterprise_PageCache/active')
                        && $version != '1.9') {
                        $response["header"] = preg_replace(
                            "#<div class=\"nav-container\">(.*?)</div>#is",
                            "", trim($header)
                        );
                    } else {
                        $response["header"] = trim($header);
                    }
                    $response["content"] = trim($content);
                }
            }
            $this->_getSession()->setCartWasUpdated(true);
        } catch (Mage_Core_Exception $e) {
            $message             = $e->getMessage();
            $response["message"] = $message;
            Mage::logException($e);
        } catch (Exception $e) {
            $message             = $this->__('Cannot update shopping cart.');
            $response["message"] = $message;
            Mage::logException($e);
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }//end updatePostAction()


}//end class