<?php 
/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUMITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUMITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   GIT:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */



/**
 * Perficient_AjaxCart_Model_Adminhtml_System_Config_Source_Rule_Type
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   Release:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */
class Perficient_AjaxCart_Model_Adminhtml_System_Config_Source_Rule_Type
{
    const NONE             = 0;
    const RELATED_PRODUCTS = 1;
    const UP_SELLS         = 2;
    const CROSS_SELLS      = 3;


    /**
     * Retrieve option values array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options   = array();
        $options[] = array(
                      'label' => Mage::helper('perficient_ajaxcart')->__('None'),
                      'value' => self::NONE,
                     );
        $options[] = array(
                      'label' => Mage::helper('perficient_ajaxcart')->__('Cross-sells'),
                      'value' => self::CROSS_SELLS,
                     );
        return $options;
    }//end toOptionArray()


}//end class
