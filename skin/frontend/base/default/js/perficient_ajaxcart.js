/**
 * PERFICIENT INDIA PVT LTD.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://shop.perficient.com/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento COMMUMITY edition
 * =================================================================
 * Perficient does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUMITY edition.
 * Perficient does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * PHP version 5.x
 *
 * @category  Perficient
 * @package   Perficient_AjaxCart
 * @author    Perficient <mukesh.soni@perficient.com>
 * @copyright 2015 PERFICIENT INDIA PVT LTD
 * @license   Private http://shop.perficient.com/license-enterprise.txt
 * @version   GIT:1.0.2
 * @link      http://www.magentocommerce.com/magento-connect/simple-ajax-cart-by-zeon-solutions.html
 */

var AjaxCart = Class.create();
AjaxCart.prototype = {
    initialize: function () {
        this.container = $$('body')[0];
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
        this.bindRemoveOnMinCart();
    },

    resetLoadWaiting: function(transport){
        this.setLoadWaiting(false);
    },
    setLoadWaiting: function(enable) {
        if (enable) {
            var container = this.container;
            container.setStyle({
                opacity:.5
            });
            Element.show('loading-mask');
            $('additional-content').update('');
        }
        else {
            var container = this.container;
            container.setStyle({
                opacity:1
            });
            Element.hide('loading-mask');
        }
    },

    toggleSelectsUnderBlock: function(block, flag){
        if(Prototype.Browser.IE){
            var selects = document.getElementsByTagName("select");
            for(var i=0; i<selects.length; i++){
                /**
                 * @todo: need check intersection
                 */
                if(flag){
                    if(selects[i].needShowOnSuccess){
                        selects[i].needShowOnSuccess = false;
                        // Element.show(selects[i])
                        selects[i].style.visibility = '';
                    }
                }
                else{
                    if(Element.visible(selects[i])){
                        // Element.hide(selects[i]);
                        //                        selects[i].style.visibility = 'hidden';
                        selects[i].needShowOnSuccess = true;
                    }
                }
            }
        }
    },

    bindRemoveOnMinCart: function() {
        if (AJAXCART_ENABLED) {
            if (typeof($$('[id="mini-cart"],[id="cart-sidebar"]')[0]) != 'undefined') {
                $$('[id="mini-cart"],[id="cart-sidebar"]')[0].select('li > div >  a[class="btn-remove"],[class="remove"]').each(
                    function(element){
                        var requestUrl = element.href;
                        var reg2    = /checkout\/cart\/ajaxDelete\/id\/(\d+)/i,
                            result2 = reg2.exec(requestUrl);
                        if (!result2 || typeof(result2[1]) == 'undefined') {
                            if (element.getAttribute('onclick')) {
                                element.setAttribute('onclick', '');
                            }
                            element.observe('click',
                                (function (event) {
                                    ajaxCart.removeFromCart(requestUrl, 1);
                                    event.preventDefault();
                                })
                            );
                        }
                    }
                );
            }
        }
    },

    showButtons: function(loginRequired) {
        if (loginRequired) {
            $('btn-login').show();
            $('btn-register').show();
        } else {
            $('btn-cart-checkout').show();
            $('btn-continue-shopping').show();
        }
    },

    hideButtons: function() {
        $('btn-login').hide();
        $('btn-register').hide();
        $('btn-cart-checkout').hide();
        $('btn-continue-shopping').hide();
    },

    center: function(elem) {
        var scrollPosition = document.viewport.getScrollOffsets();
        var width = elem.clientWidth;
        var height = elem.clientHeight;
        var H = (document.viewport.getHeight() - height)/2 + scrollPosition.top;
        var L = (document.viewport.getWidth() - width)/2 - scrollPosition.left;
        elem.setStyle({
            top: H+'px',
            left: L+'px'
        });
    },

    openMessagePopup: function(timePeriod) {
        var height = this.container.getHeight();
        $('message-popup-window-mask').setStyle({
            'height':height+'px'
        });
        this.toggleSelectsUnderBlock($('message-popup-window-mask'), false);
        Element.show('message-popup-window-mask');
        var x = (window.innerWidth / 2) - ($('message-popup-window').offsetWidth / 2);
        var y = (window.innerHeight / 2) - ($('message-popup-window').offsetHeight / 2);
        Element.show('message-popup-window');
        this.center($('message-popup-window'));
        if(timePeriod) {
            setTimeout(this.closeMessagePopup.bind(this), timePeriod*1000);
        }
    },

    closeMessagePopup: function() {
        this.toggleSelectsUnderBlock($('message-popup-window-mask'), true);
        Element.hide('message-popup-window');
        Element.hide('message-popup-window-mask');
        $('message-content').update('');
        $('additional-content').update('');
        $("message-popup-window").style.width="";
        this.hideButtons();
    },

    initializeClasses: function() {
        // Doesn't work for 1.9
        if (MAGENTO_VERSION != '1.9') {
            mainNav("nav", {
                "show_delay":"100",
                "hide_delay":"100"
            });
        }
    },

    bindDropDownEvent: function() {
        var skipLinks = jQuery('.header-minicart').children('.skip-link');
        var skipContents = jQuery('.header-minicart').children('.skip-content');
        skipLinks.on('click', function (e) {
            e.preventDefault();
            var self = jQuery(this);
            var target = self.attr('href');

            // Get target element
            var elem = jQuery(target);

            // Check if stub is open
            var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

            // Hide all stubs
            skipLinks.removeClass('skip-active');
            skipContents.removeClass('skip-active');

            // Toggle stubs
            if (isSkipContentOpen) {
                self.removeClass('skip-active');
            } else {
                self.addClass('skip-active');
                elem.addClass('skip-active');
            }
        });
        jQuery('#header-cart').on('click', '.skip-link-close', function(e) {
            var parent = jQuery(this).parents('.skip-content');
            var link = parent.siblings('.skip-link');

            parent.removeClass('skip-active');
            link.removeClass('skip-active');

            e.preventDefault();
        });
    },

    extractScripts: function(strings) {
        var scripts = strings.extractScripts();
        scripts.each(function(script){
            try {
                eval(script.replace(/var /gi, ""));
            }
            catch(e){
                if(window.console) console.log(e.name);
            }
        });
    },

    addByUrl: function(postUrl, enabled){
        if (enabled) {

            this.setLoadWaiting(true);
            var request = new Ajax.Request(
                postUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onFailure: function(response){
                        alert('An error occurred while processing your request');
                        this.onComplete;
                    },
                    onSuccess: function(response){
                        if (response && response.responseText){
                            if (typeof(response.responseText) == 'string') {
                                eval('result = ' + response.responseText);
                            }
                            if (typeof(result.message) != 'undefined' && typeof($('message-content')) != 'undefined') {
                                $('message-content').update(result.message);
                            }
                            //Update the header
                            if (typeof(result.header) != 'undefined') {
                                if (MAGENTO_VERSION == '1.9') {
                                    var header = result.header;
                                    $$('.header-minicart')[0].innerHTML = result.header;
                                    this.bindDropDownEvent();
                                } else {
                                    var begin = result.header.indexOf('<div class="header-container">') + '<div class="header-container">'.length;
                                    var end = result.header.length - '</div>'.length;
                                    var header = result.header.substring(begin,end);
                                    $$('.header-container')[0].innerHTML = header;
                                    this.extractScripts(header);
                                    this.initializeClasses();
                                }
                                this.bindRemoveOnMinCart(); //bind remove on mini cart items
                            }
                            //Update the top links
                            if (typeof(result.toplinks) != 'undefined') {
                                var header = result.toplinks;
                                $$('#header-account')[0].innerHTML = result.toplinks;
                            }
                            //Update the sidebar block
                            if (typeof(result.sidebar) != 'undefined' && typeof($$('div.'+result.block_id)[0]) != 'undefined') {
                                $$('div.'+result.block_id)[0].replace(result.sidebar);
                                this.extractScripts(result.sidebar);
                                this.initializeClasses();
                            }
                            var loginRequired = false;
                            if (typeof(result.loginRequired) != 'undefined' && result.loginRequired) {
                                loginRequired = true;
                            }
                            this.showButtons(loginRequired);
                            this.openMessagePopup(AJAX_CART.isAutoHidePopup);
                        }
                    }.bind(this)
                }
            );
        } else {
            setLocation(postUrl);
        }
    },

    removeFromCart: function(postUrl, enabled){
        var conf = confirm('Are you sure you would like to remove this item from the shopping cart?');
        if (enabled && conf) {
            this.setLoadWaiting(true);
            var request = new Ajax.Request(
                postUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onFailure: function(response){
                        alert('An error occurred while processing your request');
                        this.onComplete;
                    },
                    onSuccess: function(response){
                        if (response && response.responseText){
                            if (typeof(response.responseText) == 'string') {
                                eval('result = ' + response.responseText);
                            }
                            if (result.message) {
                                $('message-content').update(result.message);
                            }
                            //Update the header
                            if (typeof(result.header) != 'undefined') {
                                if (MAGENTO_VERSION == '1.9') {
                                    var header = result.header;
                                    $$('.header-minicart')[0].innerHTML = result.header;
                                    this.bindDropDownEvent();
                                } else {
                                    var begin = result.header.indexOf('<div class="header-container">') + '<div class="header-container">'.length;
                                    var end = result.header.length - '</div>'.length;
                                    var header = result.header.substring(begin,end);
                                    $$('.header-container')[0].innerHTML = header;
                                    this.extractScripts(header);
                                    this.initializeClasses();
                                }
                                this.bindRemoveOnMinCart(); //bind remove on mini cart items
                            }
                            //Update the content
                            if (typeof(result.content) != 'undefined') {
                                if (result.content.indexOf('<div class="cart">') == -1) {
                                    var content = result.content;
                                } else {
                                    var begin = result.content.indexOf('<div class="cart">') + '<div class="cart">'.length;
                                    var end = result.content.length - '</div>'.length;
                                    var content = result.content.substring(begin,end);
                                }
                                $$('.cart')[0].innerHTML = content;
                                this.extractScripts(content);
                                // Remove Product from cart
                                if (AJAXCART_ENABLED) {
                                    $$('#shopping-cart-table > tbody > tr > td >  a.btn-remove').each(function(element){
                                        element.observe('click',
                                            (function (event) {
                                                ajaxCart.removeFromCart(element.href, true);
                                                event.preventDefault();
                                            })
                                        );
                                    })
                                }
                            }
                            this.hideButtons();
                            this.openMessagePopup(AJAX_CART.isAutoHidePopup);
                        }
                    }.bind(this)
                }
            );
        } else if(conf) {
            setLocation(postUrl);
        } else {
            return false;
        }
    },

    viewProductOptions: function(postUrl, id, loadByUrl){
        this.closeMessagePopup();
        this.setLoadWaiting(true);
        var request = new Ajax.Request(
            BASE_URL+postUrl,
            {
                method: 'get',
                parameters: {
                    id:id,
                    loadByUrl: (loadByUrl ? loadByUrl : 0)
                },
                evalJS: true,
                onComplete: this.onComplete,
                onFailure: function(response){
                    alert('An error occurred while processing your request');
                    this.onComplete;
                },
                onSuccess: function(response){
                    try {
                        $("message-popup-window").style.width="952px";
                        if (response && response.responseText){
                            //                        response.responseText = response.responseText.replace(/(\r\n|\n|\r)/gm,"");
                            $('message-content').innerHTML = response.responseText;
                            this.extractScripts(response.responseText);
                            this.hideButtons();
                            this.openMessagePopup(0);
                        }
                    }
                    catch (error){
                        alert(error);
                    }
                }.bind(this)
            }
        );
    },

    updateCart: function(postUrl, formObj, emptyCart){
        this.setLoadWaiting(true);
        var request = new Ajax.Request(
            postUrl,
            {
                method: 'post',
                parameters: (emptyCart ? {'update_cart_action':'empty_cart'}: Form.serialize(formObj)),
                onComplete: this.onComplete,
                onFailure: function(response){
                    alert('An error occurred while processing your request');
                    this.onComplete;
                },
                onSuccess: function(response){
                    if (response && response.responseText){
                        if (typeof(response.responseText) == 'string') {
                            eval('result = ' + response.responseText);
                        }
                        if (result.message) {
                            $('message-content').update(result.message);
                        }
                        //Update the header
                        if (typeof(result.header) != 'undefined') {
                            if (MAGENTO_VERSION == '1.9') {
                                var header = result.header;
                                $$('.header-minicart')[0].innerHTML = result.header;
                                this.bindDropDownEvent();
                            } else {
                                var begin = result.header.indexOf('<div class="header-container">') + '<div class="header-container">'.length;
                                var end = result.header.length - '</div>'.length;
                                var header = result.header.substring(begin,end);
                                $$('.header-container')[0].innerHTML = header;
                                this.extractScripts(header);
                                this.initializeClasses();
                            }
                            this.bindRemoveOnMinCart(); //bind remove on mini cart items
                        }
                        //Update the content
                        if (typeof(result.content) != 'undefined') {
                            if (result.content.indexOf('<div class="cart">') == -1) {
                                var content = result.content;
                            } else {
                                var begin = result.content.indexOf('<div class="cart">') + '<div class="cart">'.length;
                                var end = result.content.length - '</div>'.length;
                                var content = result.content.substring(begin,end);
                            }
                            $$('.cart')[0].innerHTML = content;
                            this.extractScripts(content);
                            //update shopping Cart
                            if (AJAXCART_ENABLED && typeof($$('#shopping-cart-table > tfoot')[0]) != 'undefined') {
                                // If empty cart button is clicked
                                $emptyCartButton = $('empty_cart_button');
                                if ($emptyCartButton) {
                                    Event.observe($emptyCartButton, 'click', function(event)
                                    {
                                        ajaxCart.updateCart(BASE_URL+'/checkout/cart/updatePost/', this, 1);
                                        event.preventDefault();
                                    });
                                }
                                // If update cart button is clicked
                                $$('form[action$="checkout/cart/updatePost/"]')[0].observe('submit',
                                    (function(event) {
                                        ajaxCart.updateCart(BASE_URL+'/checkout/cart/updatePost/', this);
                                        event.preventDefault();
                                    })
                                );
                            }
                        }
                        this.hideButtons();
                        this.openMessagePopup(AJAX_CART.isAutoHidePopup);
                    }
                }.bind(this)
            }
        );
    },



    addUpdate: function(postUrl){
        var productForm = new VarienForm('product_addtocart_form', true);
        if (!productForm.validator.validate()) {
            return;
        }
        this.setLoadWaiting(true);
        var request = new Ajax.Request(
            BASE_URL+postUrl,
            {
                method: 'post',
                parameters: Form.serialize($('product_addtocart_form')),
                evalJS: true,
                onComplete: this.onComplete,
                onFailure: function(response){
                    alert('An error occurred while processing your request');
                    this.onComplete;
                },
                onSuccess: function(response){
                    $("message-popup-window").style.width="";
                    if (response && response.responseText){
                        if (typeof(response.responseText) == 'string') {
                            eval('result = ' + response.responseText);
                        }
                        if (typeof(result.item_id) != 'undefined') {
                            $('item_id').value = result.item_id;
                        }
                        if (result.message) {
                            $('message-content').update(result.message);
                        }

                        //Update the header
                        if (typeof(result.header) != 'undefined') {
                            if (MAGENTO_VERSION == '1.9') {
                                var header = result.header;
                                $$('.header-minicart')[0].innerHTML = result.header;
                                this.bindDropDownEvent();
                            } else {
                                var begin = result.header.indexOf('<div class="header-container">') + '<div class="header-container">'.length;
                                var end = result.header.length - '</div>'.length;
                                var header = result.header.substring(begin,end);
                                $$('.header-container')[0].innerHTML = header;
                                this.extractScripts(header);
                                this.initializeClasses();
                            }
                            this.bindRemoveOnMinCart(); //bind remove on mini cart items
                        }
                        //Update the sidebar block
                        if (typeof(result.sidebar) != 'undefined' && typeof($$('div.'+result.block_id)[0]) != 'undefined') {
                            $$('div.'+result.block_id)[0].replace(result.sidebar);
                            this.extractScripts(result.sidebar);
                            this.initializeClasses();
                        }
                        //Display additional product block
                        if (typeof(result.additional) != 'undefined' && result.additional) {
                            $('additional-content').innerHTML = result.additional;
                        }
                        var loginRequired = false;
                        if (typeof(result.loginRequired) != 'undefined' && result.loginRequired) {
                            loginRequired = true;
                        }
                        this.showButtons(loginRequired);
                        this.openMessagePopup(AJAX_CART.isAutoHidePopup);
                    }
                }.bind(this)
            }
        );
    }
};

function validateDownloadableCallback(elmId, result) {
    var container = $('downloadable-links-list');
    if (result == 'failed') {
        container.removeClassName('validation-passed');
        container.addClassName('validation-failed');
    } else {
        container.removeClassName('validation-failed');
        container.addClassName('validation-passed');
    }
}
document.observe("dom:loaded", function() {
    // Add Product to cart from detail page
    if (typeof(productAddToCartForm) != 'undefined') {
        var addToCartFormSubmit = productAddToCartForm.submit;
        productAddToCartForm.submit = function(button, url) {
            if (AJAXCART_ENABLED) {
                if (this.validator.validate()) {
                    var form = this.form;
                    var oldUrl = form.action;

                    if (url) {
                        form.action = url;
                    }
                    var e = null;
                    try {
                        ajaxCart.addUpdate(URL);
                    } catch (e) {
                    }
                    this.form.action = oldUrl;
                    if (e) {
                        throw e;
                    }

                    if (button && button != 'undefined') {
                        button.disabled = true;
                    }
                }
            } else {
                addToCartFormSubmit(button, url);
            }
        }.bind(productAddToCartForm);
    }

    // View Product details from listing page
    if (AJAXCART_ENABLED) {
        if (typeof($$('.category-products')[0]) != 'undefined') {
            $$('.category-products')[0].select('div.actions > .button').each(function(element){
                var onClick = element.getAttribute('onclick');
                var locationUrl = null;
                var reg    = /setLocation\('(.*)'\)/,
                    result = reg.exec(onClick);
                if (result && typeof(result[1]) != "undefined") {
                    locationUrl = result[1];
                    element.setAttribute('onclick', '');
                }
                element.observe('click',
                    (function (event) {
                        var requestUrl = '';
                        if (locationUrl) {
                            requestUrl = locationUrl;
                        } else {
                            requestUrl = element.href;
                        }
                        var reg    = /product\/view\/id\/(\d+)/i,
                            result = reg.exec(requestUrl);
                        var reg2    = /checkout\/cart\/add\/(.*)\/product\/(\d+)/i;
                        result2 = reg2.exec(requestUrl);
                        if (result && typeof result[1] != "undefined") {
                            ajaxCart.viewProductOptions('ajaxcart/product/view', result[1]);
                        } else if(result2 && typeof(result2[2]) != "undefined") {
                            ajaxCart.viewProductOptions('ajaxcart/product/view', result2[2]);
                        } else {
                            var productSlug = requestUrl.split("?")[0].split("/").slice(-1).pop().replace(PRODUCT_URL_SUFFIX,'');
                            if (typeof productSlug != "undefined") {
                                ajaxCart.viewProductOptions('ajaxcart/product/view', productSlug, 1);
                            }
                        }
                        event.preventDefault();
                    })
                );
            })
        }
    }

    // Remove Product from cart
    if (AJAXCART_ENABLED) {
        $$('#shopping-cart-table > tbody > tr > td >  a.btn-remove').each(function(element){
            element.observe('click',
                (function (event) {
                    ajaxCart.removeFromCart(element.href, true);
                    event.preventDefault();
                })
            );
        })
    }
    //update shopping Cart
    if (AJAXCART_ENABLED && typeof($$('#shopping-cart-table > tfoot')[0]) != 'undefined') {
        // If empty cart button is clicked
        $emptyCartButton = $('empty_cart_button');
        if ($emptyCartButton) {
            Event.observe($emptyCartButton, 'click', function(event)
            {
                ajaxCart.updateCart(BASE_URL+'/checkout/cart/updatePost/', this, 1);
                event.preventDefault();
            });
        }
        // If update cart button is clicked
        $$('form[action$="checkout/cart/updatePost/"]')[0].observe('submit',
            (function(event) {
                ajaxCart.updateCart(BASE_URL+'/checkout/cart/updatePost/', this);
                event.preventDefault();
            })
        );
    }
});
