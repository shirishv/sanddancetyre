<?php 
	error_reporting(0); 
	ini_set("display_errors", 1);
	require_once ('app/Mage.php');
	Mage::app();
?>

<?php 

$from_email = Mage::getStoreConfig('trans_email/ident_general/email'); //fetch sender email Admin
$from_name = Mage::getStoreConfig('trans_email/ident_general/name'); //fetch sender name Admin

$logo_src =   Mage::getStoreConfig('design/header/logo_src');

$logoUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN)."frontend/bitware/sanddance/".$logo_src; 

// http://103.224.243.154/magento/sanddance/skin/frontend/bitware/sanddance/images/sanddance.png

$wData = Mage::getSingleton('core/resource')->getConnection('core_write');
$resultData = $wData->query('select * from quickrfq');
$rowData = $resultData->fetchAll();
//echo"<pre>"; print_r($rowData);

foreach($rowData as $rowDataValue){
		
		$servicerequested = trim(htmlentities($rowDataValue['servicerequested']));
		$servicedatetime = trim(htmlentities($rowDataValue['servicedatetime']));
		$appointmentDate = trim(htmlentities($rowDataValue['servicedatetime']));
		$appointmentDate = trim(substr($appointmentDate,0,6));
		$contactName =  trim(htmlentities($rowDataValue['contact_name']));
		$contactEmail =  trim(htmlentities($rowDataValue['email']));
		//$contactEmail = 'ishan@bitwaretechnologies.com';
		//$appointmentDate = '20/05';	
		$dateCheck = date("d/m",strtotime("+1 day")); 
		
		if($appointmentDate == $dateCheck){
 
			$to = $contactEmail;
			$subject = "Appointment Reminder";
			// Get HTML contents from file
			//$htmlContent = file_get_contents("email_template.html");
			$htmlContent = '<div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
				<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
				<tbody>
					<tr>
						<td>
							<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
							<tbody>
								<tr>
									<td>
										<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
										<tbody>
										
											<tr>
												<td colspan="3" align="center">
													<table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
													<tbody>
														<tr>
															<td colspan="3" height="60"></td></tr><tr><td width="25"></td>
															<td align="center">
																<h1 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Sand Dance</h1>
															</td>
															<td width="25"></td>
														</tr>
														<tr>
															<td colspan="3" height="40"></td></tr><tr><td colspan="5" align="center">
																<p style="color:#404040;font-size:16px;line-height:24px;font-weight:lighter;padding:0;margin:0">
																Hello, '.$contactName.'
																</p><br>
																<p>This is to remind you that you have an appointment with us.</p>
																<p>Below are the details</p>
																<tr>
																		<td>Service Requested</td>
																		<td>Appointment Details</td>
																</tr>
																<tr>
																		<td>'.$servicerequested.'</td>
																		<td>'.$servicedatetime.'</td>
																</tr>
																<br>
																<p style="color:#404040;font-size:16px;line-height:22px;font-weight:lighter;padding:0;margin:0">Thank you, Sand Dance </p>
															</td>
														</tr>
														<tr>
														<td colspan="4">
															<div style="width:100%;text-align:center;margin:30px 0">
																<table align="center" cellpadding="0" cellspacing="0" style="font-family:HelveticaNeue-Light,Arial,sans-serif;margin:0 auto;padding:0">
																
																</table>
															</div>
														</td>
													</tr>
													<tr><td colspan="3" height="30"></td></tr>
												</tbody>
												</table>
											</td>
										</tr>
										
										
										</tbody>
										</table>
										
									</td>
								</tr>
							</tbody>
							</table>
						</td>
					</tr>
				</tbody>
				</table>
			</div>';

			// Set content-type for sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

			// Additional headers
			$headers .= 'From: '.$from_name.'<'.$from_email.'>' . "\r\n";
			
			$sendMail = mail($to,$subject,$htmlContent,$headers);
			
			if($sendMail){
				Mage::log('Mail Sent to '.$contactEmail);
			}else{
				Mage::log('Problems in sending mail to '.$contactEmail);
			}
		
		}

}

?>